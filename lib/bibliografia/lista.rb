#clase que representa la lista enlazada

Node=Struct.new(:value,:next,:before)
class Lista
    attr_reader :cabeza,:cola,:n_elementos
    def initialize()
        @cabeza=Node.new(nil,nil,nil)
        @cola=Node.new(nil,nil,nil)
        @n_elementos=0 
        
    end
   
    def inserta(item)
       
        item.each do |n|
         nodo=Node.new(nil)
         nodo.value=n
            if(@cabeza.value==nil)
                @cabeza=nodo
                @cola=nodo
                
            else
                a_cabeza=@cola
                
                while a_cabeza.next!=nil do
                    
                    a_cabeza=a_cabeza.next
                    
                end
                a_cabeza.next=nodo
                @cabeza=a_cabeza.next
                @cabeza.before=a_cabeza
            end
            @n_elementos=@n_elementos+1
       end
    end
    
    def extrae
        
        a_aux=@cola
        @cola=@cola.next
        @n_elementos=@n_elementos-1
        a_aux
    end
    def empty?
        if(@cabeza.value==nil)
            true
        else
            false
        end
    end
      

    def mostrar
        aux_cabeza=@cabeza
        aux_cola=@cola
        
        print "\n de cola hacia adelante:\n "
        print aux_cola.value
        while aux_cola.next!=nil do
            print ","
            aux_cola=aux_cola.next
            print aux_cola.value
        end
         print "\n=========================================================\n"
        print "de cabeza hacia detras:\n"
        print aux_cabeza.value
        while aux_cabeza.before!=nil do
            print ","
            aux_cabeza=aux_cabeza.before
            #aux_cabeza=aux_cabeza.next
            print aux_cabeza.value
        end
        print "\n =========================================================\n"
    end
end